project(
  'drobillad',
  ['c', 'cpp'],
  default_options: [
    'b_ndebug=if-release',
    'buildtype=release',
    'c_std=c99',
    'cpp_std=c++17',
  ],
  license: 'GPLv3+',
  meson_version: '>= 0.56.0',
  version: '0.0.0',
)

#######################
# Compilers and Flags #
#######################

# Required tools
pkg = import('pkgconfig')
cc = meson.get_compiler('c')
cpp = meson.get_compiler('cpp')

##########################
# LV2 Path Configuration #
##########################

lv2dir = get_option('lv2dir')
if lv2dir == ''
  prefix = get_option('prefix')
  if target_machine.system() == 'darwin' and prefix == '/'
    lv2dir = '/Library/Audio/Plug-Ins/LV2'
  elif target_machine.system() == 'haiku' and prefix == '/'
    lv2dir = '/boot/common/add-ons/lv2'
  elif target_machine.system() == 'windows' and prefix == 'C:/'
    lv2dir = 'C:/Program Files/Common/LV2'
  else
    lv2dir = prefix / get_option('libdir') / 'lv2'
  endif
endif

###############
# Subprojects #
###############

lv2kit = subproject('lv2kit', required: get_option('lv2kit'))

lilv_dep = lv2kit.get_variable('lilv_dep')
lv2_dep = lv2kit.get_variable('lv2_dep')
serd_dep = lv2kit.get_variable('serd_dep')
sord_dep = lv2kit.get_variable('sord_dep')
sratom_dep = lv2kit.get_variable('sratom_dep')
suil_dep = lv2kit.get_variable('suil_dep')

lv2dir = lv2_dep.get_variable(
  default_value: lv2dir,
  internal: 'lv2dir',
  pkgconfig: 'lv2dir',
  pkgconfig_define: ['prefix', '/prefix'],
)

# LV2 plugins

blop_lv2 = subproject('blop.lv2', required: get_option('plugins'))

fomp_lv2 = subproject(
  'fomp.lv2',
  default_options: ['cpp_std=c++17'],
  required: get_option('plugins'),
)

mda_lv2 = subproject(
  'mda.lv2',
  default_options: ['cpp_std=c++17'],
  required: get_option('plugins'),
)

# Libraries

ganv = subproject(
  'ganv',
  default_options: ['cpp_std=c++17'],
  required: get_option('ganv'),
)

raul = subproject('raul', required: get_option('raul'))

# Applications

ingen = subproject(
  'ingen',
  default_options: ['cpp_std=c++17'],
  required: get_option('ingen'),
)

jalv = subproject(
  'jalv',
  default_options: ['cpp_std=c++17'],
  required: get_option('jalv'),
)

# machina = subproject('machina', required: get_option('machina')))
patchage = subproject('patchage', required: get_option('patchage'))
